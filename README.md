# SPGO-WEBAPP

Proyecto para el backend de la aplicacion movil y pagina web de la SPGO, ademas de sistema de gestion interno de la misma.

En este documento, se detallan instrucciones de como realizar operaciones comunes para el manejo del proyecto.

## Entorno de desarrollo
### Levantar proyecto
* Clonar repositorio del proyecto.
```sh
$ git clone git@bitbucket.org:codeapy/spgo-webapp.git
```
* Ejecutar comando make para levantar entorno de desarrollo.
```sh
$ make up
```
### Restaurar base de datos
* Copiar backup desde el servidor hasta el directorio local, reemplazando la fecha del backup en el nombre del archivo pasado al comando scp (Asume que se tiene el nombre droplet asociado a la IP del servidor en el archivo ~/.ssh/config, caso contrario debe especificarse la IP del server).
```sh
$ scp root@droplet:~/production/backup/YYYY-MM-DD_02:00_dump.tar.gz dump.tar.gz
```
* Mover el backup traido del server a la carpeta restore.
```sh
$ sudo mv dump.tar.gz ./restore
```
* Ejecutar el comando make para restaurar la base de datos.
```sh
$ make restore-db
```
**_Nota_**: El comando anterior tambien restaura los migrations contenidos en el backup.

### Migrations
Por ahora, NO pushear migrations, estos se haran en el server a partir de los cambios en los models.

## Migracion inicial de datos de socios
Instrucciones para la carga inicial de los datos de los socios.
#### Requisitos previos
* Tener el archivo ***datos.xlsx***, con los datos personales y cuotas de todos los socios, debajo del directorio ***/src*** del proyecto.
#### Instrucciones para migrar

* Eliminar los containers, ya que habran nuevas dependencias.
* Eliminar los volumenes ya que se necesita trabajar con la base de datos vacia.

```sh
$ docker container rm -f $(docker container ls -a -q)
$ docker volume rm $(docker volume ls -q)
$ make build
$ make up
```

* Entrar al shell del container, y cargar los fixtures. (Se puede crear tambien en este momento un superuser.)

```sh
$ make shell-web
/src# ./manage.py loaddata tiposocio tipoactividad configuracion departamento ciudad historialtiposociopreciocuota periodo
```
* Entrar al shell de python, dentro del mismo, ejecutar las funciones para importar primero los socios y luego las cuotas. Estas operaciones pueden llevar un tiempo.

```sh
/src# ./manage.py shell
>>> from importsocios import *
>>> from importcuotas import *
>>> make_import_socios()
>>> make_import_cuotas()
```

## Endpoints
### list noticia
parametros
* limite_cliente: especifica el limite de la busqueda. El servidor tiene como tope 30, si es mayor al tope, retorna 30.
* orden: orden de acuerdo a la fecha de publicacion. 'ASC', 'DESC'. default: DESC
* offset: del pagination. default: 0
* principal: 'True', 'False'. indica si se traen solo las noticias principales(destacadas) o no
* categoria: para filtrar las noticias de acuerdo a la categoria. colocar el UUID de la categoria noticia
* relacionada_a: trae noticias relacionadas a la noticia. colocar el UUID de la noticia
* busqueda: trae las noticias que contienen en el nombre lo pasado
* tipo_de_categoria: filtra de acuerdo al tipo de categoria. "ACTIVIDADES", "PUBLICACIONES", "CONGRESOS"


### list submenus
parametros
* limite_cliente: especifica el limite de la busqueda. El servidor tiene como tope 30, si es mayor al tope, retorna 30.
* offset: del pagination. default: 0
* busqueda: trae las noticias que contienen en el nombre lo pasado
* uuid_padre: trae los submenus del menu padre especificado


## list actividad
### lista las actividades a las que asistio la persona
parametros
* limite_cliente: especifica el limite de la busqueda. El servidor tiene como tope 30, si es mayor al tope, retorna 30.
* offset: del pagination. default: 0
* busqueda: trae las noticias que contienen en el nombre lo pasado
* custom_persona: trae las actividades a la que asistio la persona. especificar uuid_persona
* orden: orden de acuerdo a la fecha de asistencia. 'ASC', 'DESC'. default: DESC


### list items pagina
parametros
* limite_cliente: especifica el limite de la busqueda. El servidor tiene como tope 30, si es mayor al tope, retorna 30.
* offset: del pagination. default: 0
* busqueda: trae las noticias que contienen en el titulo lo pasado
* uuid_pagina: trae los items de la pagina especificada


### list categoria
lista las categorias de noticias disponibles
parametros
* limite_cliente: especifica el limite de la busqueda. El servidor tiene como tope 30, si es mayor al tope, retorna 30.
* offset: del pagination. default: 0
* busqueda: trae las categoria noticias que contienen en el nombre lo pasado
* orden: orden de acuerdo al nombre. 'ASC', 'DESC'. default: DESC


### list item slider
lista los elementos que seran mostrados en el slider principal
parametros
* limite_cliente: especifica el limite de la busqueda. El servidor tiene como tope 30, si es mayor al tope, retorna 30.


### list menu
parametros
* es_root: 'True', 'False'. especifica  si se trae menus root(los que aparecen en el drawer), o no root. default: None (trae ambos).

### contenido contactenos
trae toda la informacion referente a la vista contactenos

### retrieve pagina
http://localhost:8000/api/v1/pagina/{uuid_pagina}/
obtiene los datos de la pagina especificada
parametros
* pasar_items_pagina: 'True','False'. especifica si se trae los items de la pagina. default: 'False'

### update usuario
http://localhost:8000/api/v1/usuario/{uuid_socio}/

### upload foto perfil (put)
http://localhost:8000/api/v1/upload-foto-perfil/{uuid_socio}/
sube la foto de perfil del socio como multipart.body

### login


### post firebase token

### post cambiar password

### retrieve datos app
http://localhost:8000/api/v1/datos_app/
obtiene los datos como la minima version que debe ser la app para que requiera una actualizacion de la misma, y el url de la ultima version, para que sugiera descargar la ultima version
parametros
* plataforma: 'android','ios'. especifica la plataforma