FROM python:3.5.6-stretch
ENV PYTHONUNBUFFERED 0
ENV C_FORCE_ROOT true
RUN mkdir /src
WORKDIR /src
ADD ./src /src
# Dependencias de python
RUN pip install -r requirements.txt
# Dependencias externas
# Concatenar todas en este run
RUN apt-get update \
    && apt-get -y install graphicsmagick \
    && rm -rf /var/lib/apt/lists/*
CMD sh start-gunicorn.sh & sh start-celery.sh;

## ATENCION #################################
## CORRER EL SCRIPT DJANGO TASKS Manualmente
#############################################
