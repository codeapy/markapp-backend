build: ## Construye las imagenes necesarias definidas en los yml.
	docker-compose build

up: ## Crea y corre en modo daemon (si es necesario hace build) los containers de cada servicio, definidos en los yml correspondientes.
	docker-compose up -d && docker exec web /bin/sh -c "python manage.py collectstatic --noinput"

up-non-daemon: ## Crea y corre en modo NO daemon (si es necesario hace build) los containers de cada servicio, definidos en los yml correspondientes. Esto permite ver los logs de todos los containers en tiempo real.
	docker-compose up

start: ## Inicia los containers de cada servicio, definidos en los yml correspondientes.
	docker-compose start

stop: ## Detiene los containers de cada servicio, definidos en los yml correspondientes.
	docker-compose stop

restart: ## Reinicia los containers de cada servicio, definidos en los yml correspondientes.
	docker-compose stop && docker-compose start

force-clean-containers: ## Fuerza la eliminacion de todos los containers del entorno dev del proyecto.
	docker container rm -f markapp-redis markapp-db markapp-web markapp-nginx

force-clean-volumes: ## Fuerza la eliminacion de todos los volumenes del entorno dev del proyecto.
	docker volume rm -f spgo-webapp_actividad-migrations-web-dev-data spgo-webapp_chat-migrations-web-dev-data spgo-webapp_common-migrations-web-dev-data spgo-webapp_contenido-migrations-web-dev-data spgo-webapp_db-dev-data spgo-webapp_facturacion-migrations-web-dev-data spgo-webapp_laboratorios-migrations-web-dev-data spgo-webapp_redis-dev-data spgo-webapp_rest_api-migrations-web-dev-data spgo-webapp_socio-migrations-web-dev-data

force-clean-all: force-clean-containers force-clean-volumes ## Fuerza la eliminacion de todos los containers y volumenes del entorno dev del proyecto.

shell-nginx: ## Entra interactivamente al bash del container nginx.
	docker exec -ti markapp-nginx bash

shell-web: ## Entra interactivamente al bash del container web.
	docker exec -ti markapp-web bash

shell-db: ## Entra interactivamente al bash del container db.
	docker exec -ti markapp-db bash

log-nginx: ## Imprime logs del container nginx.
	docker-compose logs markapp-nginx

log-web: ## Imprime logs del container web.
	docker-compose logs markapp-web

log-db: ## Imprime logs del container db.
	docker-compose logs markapp-db

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
