from datetime import datetime

import pytz
from django.contrib.admin import ListFilter, SimpleListFilter
from django.core.exceptions import ImproperlyConfigured

from common import models
from markapp import settings


class SingleTextInputFilter(ListFilter):
    """
    renders filter form with text input and submit button
    """
    parameter_name = None
    template = "admin/textinput_filter.html"

    def __init__(self, request, params, model, model_admin):
        super(SingleTextInputFilter, self).__init__(
            request, params, model, model_admin)
        if self.parameter_name is None:
            raise ImproperlyConfigured(
                "The list filter '%s' does not specify "
                "a 'parameter_name'." % self.__class__.__name__)

        if self.parameter_name in params:
            value = params.pop(self.parameter_name)
            self.used_parameters[self.parameter_name] = value

    def value(self):
        """
        Returns the value (in string format) provided in the request's
        query string for this filter, if any. If the value wasn't provided then
        returns None.
        """
        return self.used_parameters.get(self.parameter_name, None)

    def has_output(self):
        return True

    def expected_parameters(self):
        """
        Returns the list of parameter names that are expected from the
        request's query string and that will be used by this filter.
        """
        return [self.parameter_name]

    def choices(self, cl):
        all_choice = {
            'selected': self.value() is None,
            'query_string': cl.get_query_string({}, [self.parameter_name]),
            'display': ('Todo'),
        }
        return ({
            'get_query': cl.params,
            'current_value': self.value(),
            'all_choice': all_choice,
            'parameter_name': self.parameter_name
        }, )


class FechaCreatedAtFilter(SingleTextInputFilter):
    title = 'Fecha'
    parameter_name = 'created_at'

    def queryset(self, request, queryset):
        if self.value():
            try:
                fecha_array = self.value().split('/')
                fecha_array = [ int(n) for n in fecha_array]
                local_tz = pytz.timezone(settings.TIME_ZONE)

                dia_inicio = datetime(year=fecha_array[2], month=fecha_array[1], day=fecha_array[0], hour=0, minute=0,
                                      second=0, microsecond=0, tzinfo=local_tz)

                dia_fin = datetime(year=fecha_array[2], month=fecha_array[1], day=fecha_array[0], hour=23, minute=59,
                                      second=59, microsecond=59, tzinfo=local_tz)

                return queryset.filter(created_at__gte=dia_inicio, created_at__lte=dia_fin)
            except Exception as e:
                return queryset

class ClaseFilter(SimpleListFilter):
    title = 'Clase' # a label for our filter
    parameter_name = 'horario__clase__materia__nombre' # you can put anything here

    def lookups(self, request, model_admin):
        result = models.Clase.objects.distinct()
        return [(str(clase.id), str(clase)) for clase in result]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(horario__clase__materia_id=int(self.value()))
