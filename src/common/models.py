import requests
import uuid
from django.utils import timezone
import datetime
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.conf import settings
from django.db.models import signals
from django.dispatch import receiver
from django.core.validators import RegexValidator
from django_rest_passwordreset.signals import reset_password_token_created
from rest_framework.authtoken.models import Token
from django.core.validators import MaxValueValidator, MinValueValidator

SEXO = (
        ('MASCULINO', 'Masculino'),
        ('FEMENINO', 'Femenino'),
       )

TIPO_TELEFONO = (
        ('CELULAR', 'Celular'),
        ('LINEA_BAJA', 'Linea Baja'),
       )

DIA_SEMANA = (
        ('LUNES', 'Lunes'),
        ('MARTES', 'Martes'),
        ('MIERCOLES', 'Miércoles'),
        ('JUEVES', 'Jueves'),
        ('VIERNES', 'Viernes'),
        ('SABADO', 'Sábado'),
        ('DOMINGO', 'Domingo'),
       )

TIPO_NOTIFICACION = (
        ('Aula', 'Aula'),
        ('Aula Dirigido', 'Aula Dirigido'),
        ('Público', 'Público'),
        ('Público Dirigido', 'Público Dirigido'),
        ('General', 'General'),
        ('General Dirigido', 'General Dirigido'),
       )

YES_NO = (
    (None, ''),
    (True, 'Si'),
    (False, 'No'),
)


class Departamento(models.Model):
    """
        Modelo de un departamento
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    codigo = models.CharField(max_length=45, blank=False, null=False, unique=True)
    nombre = models.CharField(max_length=255, blank=False, null=False, unique=True)

    def __str__(self):
        return self.nombre


class Ciudad(models.Model):
    """
        Modelo de una ciudad
    """
    class Meta:
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    codigo = models.CharField(max_length=45, blank=False, null=False)
    nombre = models.CharField(max_length=255, blank=False, null=False)
    departamento = models.ForeignKey(Departamento, blank=False, null=False, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre


class Usuario(AbstractUser):
    """
        Modelo de un usuario del sistema
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    def __str__(self):
        return '{}'.format(self.username)


class Persona(models.Model):
    """
        Modelo de una persona
    """
    class Meta:
        ordering = ['nombre']
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    usuario = models.OneToOneField(Usuario, null=True, blank=True, on_delete=models.PROTECT)
    nombre = models.CharField(max_length=255, blank=False, null=False)
    apellido = models.CharField(max_length=255, blank=False, null=False)
    ci = models.CharField(max_length=255, blank=False, null=False, unique=True)
    sexo = models.CharField(max_length=9, choices=SEXO, blank=False, null=False, default='MASCULINO')
    direccion = models.CharField(max_length=255, blank=True, null=True)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.SET_NULL, blank=True, null=True)
    fecha_nacimiento = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now=True, blank=False, null=False)

    @property
    def telefonos(self):
        return self.telefono_set.all()

    @property
    def emails(self):
        return self.email_set.all()

    def __str__(self):
        return '{}'.format(self.ci + " - " + self.nombre + " " + self.apellido)


class Email(models.Model):
    """
        Modelo de una direccion de correo electronico
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    email = models.EmailField(blank=False, null=False, unique=True)
    persona = models.ForeignKey(Persona, blank=False, null=False, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now=True, blank=False, null=False)

    def __str__(self):
        return str(self.email)


class Telefono(models.Model):
    """
        Modelo de un numero telefonico
    """
    class Meta:
        verbose_name = 'Teléfono'
        verbose_name_plural = 'Teléfonos'
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    numero = models.CharField(max_length=255, blank=False, null=False)
    tipo = models.CharField(max_length=10, choices=TIPO_TELEFONO, blank=False, null=False, default='CELULAR')
    persona = models.ForeignKey(Persona, blank=False, null=False, on_delete=models.CASCADE)
    descripcion = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now=True, blank=False, null=False)

    def __str__(self):
        return self.numero


class Profesor(models.Model):
    """
    Modelo de un profesor
    """
    class Meta:
        verbose_name = 'Profesor'
        verbose_name_plural = 'Profesores'

    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    persona = models.OneToOneField(Persona, null=False, blank=False, on_delete=models.PROTECT)

    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now=True, blank=False, null=False)

    @property
    def clases(self):
        return self.clase_set.all()

    def __str__(self):
        return '{}'.format(self.persona.nombre + ' ' + self.persona.apellido)


class Alumno(models.Model):
    """
    Modelo de un alummno
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    persona = models.OneToOneField(Persona, null=False, blank=False, on_delete=models.PROTECT)

    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now=True, blank=False, null=False)

    @property
    def inscripciones(self):
        return self.inscripcion_set.all()

    def __str__(self):
        return 'Alumno: {}. Cedula: {}'.format(self.persona.nombre + ' ' + self.persona.apellido, self.persona.ci)


class Materia(models.Model):
    """
        Modelo de una materia
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    nombre = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return 'Materia: {}'.format(self.nombre)


class Aula(models.Model):
    """
        Modelo de un aula
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    nombre = models.CharField(max_length=255, blank=False, null=False)

    @property
    def beacons(self):
        return self.beacon_set.all()

    def __str__(self):
        return 'Aula: {}'.format(self.nombre)


class Beacon(models.Model):
    """
        Modelo de un beacon
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    beacon_id = models.CharField(max_length=255, blank=False, null=False)
    mac = models.CharField(max_length=255, blank=False, null=False)
    aula = models.ForeignKey(Aula, blank=True, null=True, on_delete=models.SET_NULL)


    def __str__(self):
        return 'Beacon: {}'.format(self.beacon_id)


class Notificacion(models.Model):
    """
        Modelo de una Notificacion
    """
    class Meta:
        ordering = ['-id']
        verbose_name = 'Notificación'
        verbose_name_plural = 'Notificaciones'

    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    beacon = models.ForeignKey(Beacon, blank=True, null=True, on_delete=models.SET_NULL)
    alumno = models.ForeignKey(Alumno, blank=True, null=True, on_delete=models.SET_NULL)
    titulo = models.CharField(max_length=255, blank=False, null=False)
    mensaje = models.CharField(max_length=255, blank=False, null=False)
    fecha_inicio = models.DateTimeField(blank=False, null=False, editable=False)
    fecha_fin = models.DateTimeField(blank=False, null=False)
    tipo = models.CharField(max_length=20, choices=TIPO_NOTIFICACION, blank=False, null=False, default='Aula', editable=False)

    def save(self, *args, **kwargs):
        self.fecha_inicio = datetime.datetime.now()
        if self.beacon is None:
            if self.alumno is None:
                self.tipo = 'General'
            else:
                self.tipo = 'General Dirigido'
        else:
            if self.beacon.aula is None:
                if self.alumno is None:
                    self.tipo = 'Público'
                else:
                    self.tipo = 'Público Dirigido'
            else:
                if self.alumno is None:
                    self.tipo = 'Aula'
                else:
                    self.tipo = 'Aula Dirigido'

        super(Notificacion, self).save(*args, **kwargs)

    def __str__(self):
        return '{} ({} - {})'.format(self.titulo, self.fecha_inicio.strftime("%d/%m/%Y %H:%M"), self.fecha_fin.strftime("%d/%m/%Y %H:%M"))


class Clase(models.Model):
    """
        Modelo de un beacon
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    seccion = models.CharField(max_length=255, blank=False, null=False)
    materia = models.ForeignKey(Materia, blank=False, null=False, on_delete=models.PROTECT)
    profesor = models.ForeignKey(Profesor, blank=False, null=False, on_delete=models.PROTECT)

    @property
    def horarios(self):
        return self.horario_set.all()

    def __str__(self):
        return 'Materia: {} | Seccion {} | Profesor {}'.format(self.materia.nombre, self.seccion, self.profesor.__str__())


class Horario(models.Model):
    """
    Modelo de un horario
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    dia = models.CharField(max_length=7, choices=DIA_SEMANA, blank=False, null=False)
    hora_inicio = models.CharField(max_length=5, blank=False, null=False, validators=[
            RegexValidator(
                regex='^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$',
                message='Hora de inicio no cumple formato hh:mm 24hs',
            ),
        ])
    hora_fin = models.CharField(max_length=5, blank=False, null=False, validators=[
        RegexValidator(
            regex='^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$',
            message='Hora de inicio no cumple formato hh:mm 24hs',
        ),
    ])
    clase = models.ForeignKey(Clase, blank=False, null=False, on_delete=models.CASCADE)
    aula = models.ForeignKey(Aula, blank=True, null=True, on_delete=models.DO_NOTHING)
    # porcentaje de veces esta en rango de un beacon para considerar como asistencia
    porcentaje = models.IntegerField(blank=False, null=False, default=60, validators=[
            MaxValueValidator(100),
            MinValueValidator(1)
        ])

    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now=True, blank=False, null=False)

    def __str__(self):
        return '{} | Dia {} | Desde {} | Hasta {}'\
            .format(str(self.clase), self.dia, self.hora_inicio, self.hora_fin)

    @property
    def duracion(self):
        hora_inicio_parts = self.hora_inicio.split(':')
        print(str(hora_inicio_parts))
        hora_inicio_horas = int(hora_inicio_parts[0])
        hora_inicio_minutos = int(hora_inicio_parts[1])
        total_minutos_hora_inicio = hora_inicio_horas * 60 + hora_inicio_minutos

        hora_fin_parts = self.hora_fin.split(':')
        print(str(hora_fin_parts))
        hora_fin_horas = int(hora_fin_parts[0])
        hora_fin_minutos = int(hora_fin_parts[1])
        total_minutos_hora_fin = hora_fin_horas * 60 + hora_fin_minutos

        return int(total_minutos_hora_fin) - int(total_minutos_hora_inicio)


class Asistencia(models.Model):
    """
        Modelo de una asistencia
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    alumno = models.ForeignKey(Alumno, blank=False, null=False, on_delete=models.PROTECT)
    horario = models.ForeignKey(Horario, blank=False, null=False, on_delete=models.PROTECT)

    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now=True, blank=False, null=False)

    def __str__(self):
        return '{} || {}'.format(str(self.alumno), str(self.horario))


class Inscripcion(models.Model):
    """
        Modelo de una inscripcion
    """
    class Meta:
        verbose_name = 'Inscripción'
        verbose_name_plural = 'Inscripciones'
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    alumno = models.ForeignKey(Alumno, blank=False, null=False, on_delete=models.PROTECT)
    clase = models.ForeignKey(Clase, blank=False, null=False, on_delete=models.PROTECT)

    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now=True, blank=False, null=False)


    def __str__(self):
        return '{} || {}'.format(str(self.alumno), str(self.clase))

class Configuracion(models.Model):
    """
        Modelo de una configuracion
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    descripcion = models.CharField(max_length=500, blank=False, null=False)
    clave = models.CharField(max_length=255, blank=False, null=False, unique=True)
    valor = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.descripcion+" "+self.clave


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

@receiver(reset_password_token_created)
def password_reset_token_created(sender, reset_password_token, *args, **kwargs):
    send_password_reset_email.delay(reset_password_token.user.id, reset_password_token.key)

