"""
Valores constantes de opciones, dropdowns, selects, etc.
"""
from common.models import SEXO, DIA_SEMANA
from django.db.utils import OperationalError


sexo_values = {item[0]: item[1] for item in SEXO}
dia_semana_values = {item[0]: item[1] for item in DIA_SEMANA}
