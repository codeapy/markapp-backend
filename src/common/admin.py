import csv
from datetime import datetime

import pytz
from django.contrib import admin
from django.utils import timezone

# Register your models here.
from django.contrib.admin import SimpleListFilter
from django.http import HttpResponse

from common import models
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from rest_framework.authtoken.models import Token

from common.filters import SingleTextInputFilter, FechaCreatedAtFilter
from markapp import settings


class UsuarioAdmin(UserAdmin):
    pass


admin.site.register(models.Usuario, UsuarioAdmin)


class PersonaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido', 'sexo', 'ci')


admin.site.register(models.Persona, PersonaAdmin)


class EmailAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Email, EmailAdmin)


class TelefonoAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Telefono, TelefonoAdmin)


class DepartamentoAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Departamento, DepartamentoAdmin)


class CiudadAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Ciudad, CiudadAdmin)


class ProfesorAdmin(admin.ModelAdmin):
    list_display = ('get_nombre', 'get_apellido', 'get_sexo', 'get_ci')

    def get_nombre(self, obj):
        return obj.persona.nombre

    get_nombre.short_description = 'Nombre'
    get_nombre.admin_order_field = 'persona__nombre'

    def get_apellido(self, obj):
        return obj.persona.apellido

    get_apellido.short_description = 'Apellido'
    get_apellido.admin_order_field = 'persona__apellido'

    def get_sexo(self, obj):
        return obj.persona.sexo

    get_sexo.short_description = 'Sexo'
    get_sexo.admin_order_field = 'persona__sexo'

    def get_ci(self, obj):
        return obj.persona.ci

    get_ci.short_description = 'Cedula'
    get_ci.admin_order_field = 'persona__ci'

admin.site.register(models.Profesor, ProfesorAdmin)

class AlumnoAdmin(admin.ModelAdmin):
    list_display = ('get_nombre', 'get_apellido', 'get_sexo', 'get_ci')

    def get_nombre(self, obj):
        return obj.persona.nombre

    get_nombre.short_description = 'Nombre'
    get_nombre.admin_order_field = 'persona__nombre'

    def get_apellido(self, obj):
        return obj.persona.apellido

    get_apellido.short_description = 'Apellido'
    get_apellido.admin_order_field = 'persona__apellido'

    def get_sexo(self, obj):
        return obj.persona.sexo

    get_sexo.short_description = 'Sexo'
    get_sexo.admin_order_field = 'persona__sexo'

    def get_ci(self, obj):
        return obj.persona.ci

    get_ci.short_description = 'Cedula'
    get_ci.admin_order_field = 'persona__ci'

admin.site.register(models.Alumno, AlumnoAdmin)

class MateriaAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Materia, MateriaAdmin)

class AulaAdmin(admin.ModelAdmin):
    list_display = ('id','get_aula')

    def get_aula(self, obj):
        return obj.nombre

    get_aula.short_description = 'Nombre'
    get_aula.admin_order_field = 'nombre'

admin.site.register(models.Aula, AulaAdmin)

class BeaconAdmin(admin.ModelAdmin):
    list_display = ('get_nombre', 'get_aula', 'mac')

    def get_nombre(self, obj):
        return obj.beacon_id

    get_nombre.short_description = 'Nombre'
    get_nombre.admin_order_field = 'beacon_id'

    def get_aula(self, obj):
        return obj.aula.nombre if obj.aula else '-'

    get_aula.short_description = 'Aula'
    get_aula.admin_order_field = 'aula__nombre'


admin.site.register(models.Beacon, BeaconAdmin)

class ClaseAdmin(admin.ModelAdmin):
    list_display = ('get_materia', 'get_profesor', 'get_seccion')

    def get_materia(self, obj):
        return obj.materia.nombre

    get_materia.short_description = 'Materia'
    get_materia.admin_order_field = 'materia__nombre'

    def get_profesor(self, obj):
        return '{} {}'.format(obj.profesor.persona.nombre, obj.profesor.persona.apellido)

    get_profesor.short_description = 'Profesor'
    get_profesor.admin_order_field = 'profesor__persona__nombre'

    def get_seccion(self, obj):
        return obj.seccion

    get_seccion.short_description = 'Sección'
    get_seccion.admin_order_field = 'seccion'

admin.site.register(models.Clase, ClaseAdmin)

class HorarioAdmin(admin.ModelAdmin):
    list_display = ('get_materia', 'get_profesor', 'get_seccion', 'get_dia', 'get_hora_inicio', 'get_hora_fin')

    def get_materia(self, obj):
        return obj.clase.materia.nombre

    get_materia.short_description = 'Materia'
    get_materia.admin_order_field = 'clase__materia__nombre'

    def get_profesor(self, obj):
        return '{} {}'.format(obj.clase.profesor.persona.nombre, obj.clase.profesor.persona.apellido)

    get_profesor.short_description = 'Profesor'
    get_profesor.admin_order_field = 'clase__profesor__persona__nombre'

    def get_seccion(self, obj):
        return obj.clase.seccion

    get_seccion.short_description = 'Sección'
    get_seccion.admin_order_field = 'clase__seccion'

    def get_dia(self, obj):
        return obj.dia

    get_dia.short_description = 'Día'
    get_dia.admin_order_field = 'dia'

    def get_hora_inicio(self, obj):
        return obj.hora_inicio

    get_hora_inicio.short_description = 'Inicio'
    get_hora_inicio.admin_order_field = 'hora_inicio'

    def get_hora_fin(self, obj):
        return obj.hora_fin

    get_hora_fin.short_description = 'Fin'
    get_hora_fin.admin_order_field = 'hora_fin'


admin.site.register(models.Horario, HorarioAdmin)

from django.utils import timezone


class AsistenciaAdmin(admin.ModelAdmin):
    list_display = ('get_materia', 'get_profesor', 'get_nombre', 'get_apellido', 'get_ci', 'get_fecha')
    list_filter = (FechaCreatedAtFilter, 'horario__clase')
    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format('Asistencia')
        writer = csv.writer(response)

        field_names = [self.get_materia.short_description, self.get_profesor.short_description,
                       self.get_nombre.short_description, self.get_apellido.short_description,
                       self.get_ci.short_description, self.get_fecha.short_description]

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([self.get_materia(obj), self.get_profesor(obj),
                                   self.get_nombre(obj), self.get_apellido(obj),
                                   self.get_ci(obj), self.get_fecha(obj)])

        return response

    export_as_csv.short_description = "Descargar Excel"
    
    def get_materia(self, obj):
        return obj.horario.clase.materia.nombre

    get_materia.short_description = 'Materia'
    get_materia.admin_order_field = 'horario__clase__materia__nombre'

    def get_profesor(self, obj):
        return '{} {}'.format(obj.horario.clase.profesor.persona.nombre, obj.horario.clase.profesor.persona.apellido)

    get_profesor.short_description = 'Profesor'
    get_profesor.admin_order_field = 'horario__clase__profesor__persona__nombre'

    def get_nombre(self, obj):
        return obj.alumno.persona.nombre

    get_nombre.short_description = 'Nombre'
    get_nombre.admin_order_field = 'alumno__persona__nombre'

    def get_apellido(self, obj):
        return obj.alumno.persona.apellido

    get_apellido.short_description = 'Apellido'
    get_apellido.admin_order_field = 'alumno__persona__apellido'

    def get_ci(self, obj):
        return obj.alumno.persona.ci

    get_ci.short_description = 'Cedula'
    get_ci.admin_order_field = 'alumno__persona__ci'

    def get_fecha(self, obj):
        return timezone.localtime(obj.created_at).strftime("%d/%m/%Y %H:%M")

    get_fecha.short_description = 'Fecha'
    get_fecha.admin_order_field = 'created_at'


admin.site.register(models.Asistencia, AsistenciaAdmin)

class InscripcionAdmin(admin.ModelAdmin):
    list_display = ('get_nombre', 'get_apellido', 'get_ci', 'get_materia', 'get_profesor', 'get_seccion')

    def get_materia(self, obj):
        return obj.clase.materia.nombre

    get_materia.short_description = 'Materia'
    get_materia.admin_order_field = 'horario__clase__materia__nombre'

    def get_profesor(self, obj):
        return '{} {}'.format(obj.clase.profesor.persona.nombre, obj.clase.profesor.persona.apellido)

    get_profesor.short_description = 'Profesor'
    get_profesor.admin_order_field = 'horario__clase__profesor__persona__nombre'

    def get_nombre(self, obj):
        return obj.alumno.persona.nombre

    get_nombre.short_description = 'Nombre'
    get_nombre.admin_order_field = 'alumno__persona__nombre'

    def get_apellido(self, obj):
        return obj.alumno.persona.apellido

    get_apellido.short_description = 'Apellido'
    get_apellido.admin_order_field = 'alumno__persona__apellido'

    def get_ci(self, obj):
        return obj.alumno.persona.ci

    get_ci.short_description = 'Cedula'
    get_ci.admin_order_field = 'alumno__persona__ci'

    def get_seccion(self, obj):
        return obj.clase.seccion

    get_seccion.short_description = 'Sección'
    get_seccion.admin_order_field = 'clase__seccion'

admin.site.register(models.Inscripcion, InscripcionAdmin)

class NotificacionAdmin(admin.ModelAdmin):
    list_display = ('get_titulo', 'get_beacon', 'get_aula', 'get_fecha_inicio', 'get_fecha_fin', 'tipo')

    def get_titulo(self, obj):
        return obj.titulo

    get_titulo.short_description = 'Título'
    get_titulo.admin_order_field = 'titulo'

    def get_beacon(self, obj):
        return obj.beacon.beacon_id if obj.beacon else '-'

    get_beacon.short_description = 'Beacon'
    get_beacon.admin_order_field = 'beacon__beacon_id'

    def get_aula(self, obj):
        return obj.beacon.aula.nombre if obj.beacon and obj.beacon.aula else '-'

    get_aula.short_description = 'Aula'
    get_aula.admin_order_field = 'beacon__aula__nombre'

    def get_fecha_inicio(self, obj):
        return timezone.localtime(obj.fecha_inicio).strftime("%d/%m/%Y %H:%M")

    get_fecha_inicio.short_description = 'Fecha Inicio'
    get_fecha_inicio.admin_order_field = 'fecha_inicio'

    def get_fecha_fin(self, obj):
        return timezone.localtime(obj.fecha_fin).strftime("%d/%m/%Y %H:%M")

    get_fecha_fin.short_description = 'Fecha Fin'
    get_fecha_fin.admin_order_field = 'fecha_fin'


admin.site.register(models.Notificacion, NotificacionAdmin)
