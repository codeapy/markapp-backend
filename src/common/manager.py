import datetime

from django.db.models import Q

from common.models import Alumno, Inscripcion, Horario, Notificacion, Beacon




def get_horarios_alumno(queryset=None, alumno_uuid=None):
    """
    Retorna los horarios de las materias en que un alumno se encuentra inscripto.
    :param queryset: queryset de horarios base a partir del cual se filtra.
    :param alumno_uuid: uuid de la alumno que se quiere saber sus horarios.
    :return: queryset con los horarios del alumno.
    """
    try:
        if Alumno.objects.filter(uuid=alumno_uuid).exists():
            clases_ids = Inscripcion.objects.filter(alumno__uuid=alumno_uuid).values_list('clase', flat=True)
    except:
        pass

    if queryset is not None:
        queryset = queryset.filter(clase_id__in=clases_ids)
    else:
        queryset = Horario.objects.filter(clase_id__in=clases_ids)
    return queryset


# def get_notificaciones_alumno(queryset=None, alumno_uuid=None):
#     """
#     Retorna los horarios de las materias en que un alumno se encuentra inscripto.
#     :param queryset: queryset de horarios base a partir del cual se filtra.
#     :param alumno_uuid: uuid de la alumno que se quiere saber sus horarios.
#     :return: queryset con los horarios del alumno.
#     """
#     try:
#         if Alumno.objects.filter(uuid=alumno_uuid).exists():
#             clases_ids = Inscripcion.objects.filter(alumno__uuid=alumno_uuid).values_list('clase', flat=True)
#     except:
#         pass
#
#     if queryset is not None:
#         queryset = queryset.filter(clase_id__in=clases_ids)
#     else:
#         queryset = Horario.objects.filter(clase_id__in=clases_ids)
#     return queryset


def get_notificaciones_alumno(queryset=None, alumno_uuid=None, todos='False', fecha_ultimo_request=None):
    """
    Retorna las notificaciones de de las materias en que un alumno se encuentra inscripto.
    :param queryset: queryset de notificaciones base a partir del cual se filtra.
    :param alumno_uuid: uuid de la alumno que se quiere saber sus notificaciones.
    :param todos: ignorar decha de vigencia de la notificacion.
    :param fecha_ultimo_request: fecha de la ultima vez que el alumno pidio sus notificaciaone.
    :return: queryset con los horarios del alumno.
    """
    try:
        beacons_aula_ids = get_horarios_alumno(alumno_uuid=alumno_uuid).values_list('aula__beacon', flat=True)
        beacons_publicos_ids = Beacon.objects.filter(aula__isnull=True).values_list('id', flat=True)
        beacons_ids = list(beacons_aula_ids) + list(beacons_publicos_ids)

        now = datetime.datetime.now()

        if queryset is not None:
            queryset = queryset.filter(
                Q(
                    # General
                    Q(Q(beacon__isnull=True) & Q(alumno__isnull=True))
                    |
                    # General Dirigido (A este alumno)
                    Q(Q(beacon__isnull=True) & Q(alumno__uuid=alumno_uuid))
                )
                |
                Q(
                    # Aula o Publico
                    Q(Q(beacon__id__in=beacons_ids) & Q(alumno__isnull=True))
                    |
                    # Aula Dirigido o Publico Dirigido (A este alumno)
                    Q(Q(beacon__id__in=beacons_ids) & Q(alumno__uuid=alumno_uuid))
                )
            )

            if fecha_ultimo_request is not None and todos == 'False':
                # Que no haya visto todavia
                queryset = queryset.filter(fecha_inicio__gt=fecha_ultimo_request)
                # Que el momento del request se encuentro dentro del rango de notificacion
                queryset = queryset.filter(fecha_inicio__lt=now, fecha_fin__gt=now)
        else:
            print('-------------------------------------------------')
            print('beacons_aula_ids: {}'.format(str(beacons_aula_ids)))
            print('beacons_publicos_ids: {}'.format(str(beacons_publicos_ids)))

            print('fecha_ultimo_request: {}'.format(str(fecha_ultimo_request)))
            print('now: {}'.format(str(now)))
            print('-------------------------------------------------')


            queryset = Notificacion.objects.filter(
                Q(
                    # General
                    Q(Q(beacon__isnull=True) & Q(alumno__isnull=True))
                    |
                    # General Dirigido (A este alumno)
                    Q(Q(beacon__isnull=True) & Q(alumno__uuid=alumno_uuid))
                )
                |
                Q(
                    # Aula o Publico
                    Q(Q(beacon__id__in=beacons_ids) & Q(alumno__isnull=True))
                    |
                    # Aula Dirigido o Publico Dirigido (A este alumno)
                    Q(Q(beacon__id__in=beacons_ids) & Q(alumno__uuid=alumno_uuid))
                )
            )

            if fecha_ultimo_request is not None and todos == 'False':
                # Que no haya visto todavia
                queryset = queryset.filter(fecha_inicio__gt=fecha_ultimo_request)
                # Que el momento del request se encuentro dentro del rango de notificacion
                queryset = queryset.filter(fecha_inicio__lt=now, fecha_fin__gt=now)

        return queryset
    except Exception as e:
        print( 'exception! {}'.format(str(e)) )
        return Notificacion.objects.none()
