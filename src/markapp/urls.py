from django.conf.urls import url, include, static
from django.conf import settings
from django.contrib import admin
from rest_framework.documentation import include_docs_urls
from common import views

urlpatterns = static.static('/media/', document_root=settings.MEDIA_ROOT)

urlpatterns += [
    url(r'^admin/', admin.site.urls),
    #url(r'^api-docs/v1/', include_docs_urls(title='SPGO API')),
    url(r'^api/v1/', include('rest_api.api.v1.urls', namespace='markapp-api')),
]

admin.site.site_header = "Administración de FIUNA"
admin.site.site_title = "Portal de Administración de FIUNA"
admin.site.index_title = "Bienvenido al Portal de Administración de FIUNA"


