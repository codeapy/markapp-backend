#!/usr/bin/env bash

# ATENCION!! ES NECESARIO CORRER ESTO
# Tareas comunes que hay que realizar al deployar
# Hacer esto llamando al script o manualmente.

python manage.py collectstatic --no-input --clear
python manage.py makemigrations
python manage.py migrate --run-syncdb

#python manage.py loaddata departamento ciudad configuracion tiposocio tipoactividad historialpreciotiposocio periodo servicio
