import re
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import permissions
from common.models import Usuario, Persona


class IsOwnerOrStaff(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # All permissions granted to admin user
        # so we'll always allow GET, HEAD or OPTIONS requests.
        token = request.META.get('HTTP_AUTHORIZATION', '')[6:]

        auth_token = None
        # Si el objeto pasado es un usuario
        try:
            auth_token = str(obj.auth_token)
        except AttributeError: # Si es una persona
            try:
                qs = Usuario.objects.filter(persona=obj)
                if qs.count() > 0:
                    requesting_user = qs[0]
                    auth_token = str(requesting_user.auth_token)
            except ValueError: # Si es un socio
                qs = Usuario.objects.filter(persona=obj.persona)
                if qs.count() > 0:
                    requesting_user = qs[0]
                    auth_token = str(requesting_user.auth_token)

        is_owner = auth_token == token
        requesting_user = None
        qs = Usuario.objects.filter(auth_token=token)
        if qs.count() > 0:
            requesting_user = qs[0]
        is_staff = False
        if requesting_user:
            is_staff = requesting_user.is_staff


        # Instance must have an attribute named `owner`.
        return is_owner or is_staff


class IsStaff(permissions.BasePermission):
    def has_permission(self, request, view):
        token = request.META.get('HTTP_AUTHORIZATION', '')[6:]

        try:
            user = Usuario.objects.get(auth_token=token)
        except Usuario.DoesNotExist:
            user = None

        return user.is_staff if user else False


class IsAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        token = request.META.get('HTTP_AUTHORIZATION', '')[6:]

        try:
            user = Usuario.objects.get(auth_token=token)
        except Usuario.DoesNotExist:
            user = None

        return user.is_superuser if user else False


class AsistenteActividadOrAdmin(permissions.BasePermission):
    def has_permission(self, request, view):

        if IsAdmin().has_permission(request, None):
            return True

        token = request.META.get('HTTP_AUTHORIZATION', '')[6:]

        try:
            user = Usuario.objects.get(auth_token=token)
        except Usuario.DoesNotExist:
            user = None

        if user:
            asistente_buscado = request.GET.get('custom_persona', None)
            if not asistente_buscado or not token:
                return False
            return str(user.persona.uuid) == asistente_buscado
        else:
            return False


class IsOwnerSocioUUIDOrStaff(permissions.BasePermission):
    def has_permission(self, request, view):

        if IsStaff().has_permission(request, None):
            return True

        token = request.META.get('HTTP_AUTHORIZATION', '')[6:]

        try:
            user = Usuario.objects.get(auth_token=token)
        except Usuario.DoesNotExist:
            user = None

        if user:
            socio_uuid = request.GET.get('socio_uuid', None)

            if not socio_uuid or not token:
                return False
            return str(user.persona.socio.uuid) == socio_uuid
        else:
            return False


class IsOwnerPersonaUUIDOrStaff(permissions.BasePermission):
    def has_permission(self, request, view):

        if IsStaff().has_permission(request, None):
            return True

        token = request.META.get('HTTP_AUTHORIZATION', '')[6:]

        r = re.compile('[0-9A-Za-z_\-]{36}')
        url_parts = request.get_full_path().split('/')
        try:
            persona_uuid = list(filter(r.match, url_parts))[0]
            persona = Persona.objects.get(uuid=persona_uuid)
        except (Persona.DoesNotExist, IndexError):
            persona = None

        try:
            user = Usuario.objects.get(auth_token=token)
        except Usuario.DoesNotExist:
            user = None

        print('# persona: '+str(persona))
        print('# user: ' + str(user))

        if user:
            if not persona or not token:
                return False
            print('# user.persona.uuid: ' + str(user.persona.uuid))
            print('# persona.uuid: ' + str(persona.uuid))
            return str(user.persona.uuid) == str(persona.uuid)
        else:
            return False





