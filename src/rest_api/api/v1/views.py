import pytz
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.urls import reverse
from datetime import timedelta, datetime
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.db import transaction, DatabaseError
from django.db.models import F, Sum, FloatField
from django.shortcuts import get_object_or_404, render
from django.utils.http import urlsafe_base64_decode
from django.contrib import messages
from rest_framework import permissions, viewsets, status
from rest_framework.exceptions import ValidationError
from rest_framework.decorators import action
from django.core.exceptions import ValidationError as CoreValidationError
from common import models as common_models
from rest_api.api.v1 import serializers
from rest_api.api.v1 import permissions as custom_permissions
from django.utils.encoding import force_bytes, force_text

from common.tokens import account_activation_token
from common.models import Usuario, Horario, Alumno, Inscripcion, Profesor, Clase, Asistencia, Beacon, Materia, Notificacion
from common import manager

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.exceptions import ParseError
from rest_framework.parsers import FileUploadParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from django_rest_passwordreset.serializers import EmailSerializer, PasswordTokenSerializer
from django_rest_passwordreset.models import ResetPasswordToken
from django_rest_passwordreset.signals import reset_password_token_created, pre_password_reset, post_password_reset



import dateutil.parser

import requests
from hashlib import md5

from markapp import settings

from django.db.models import Sum



class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = serializers.CustomAuthTokenSerializer(data=request.data,
                                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']

        token, created = Token.objects.get_or_create(user=user)

        persona = common_models.Persona.objects.filter(usuario__id=user.id).first()
        persona_uuid = persona.uuid if not persona is None else None

        alumno_uuid = persona.alumno.uuid if persona.alumno is not None else None

        return Response({
            'token': token.key,
            'usuario_uuid': user.uuid,
            'persona_uuid': persona_uuid,
            'alumno_uuid': alumno_uuid,
        }, status=status.HTTP_200_OK)


class HorarioViewSet(viewsets.ModelViewSet):
    errors = []
    serializer_class = serializers.HorarioSerializer
    """
    ViewSet para operaciones sobre socios.
    """

    def get_queryset(self):
        alumno_uuid = self.request.GET.get('alumno_uuid', None)

        queryset = Horario.objects.all()

        try:
            if alumno_uuid:
                queryset = manager.get_horarios_alumno(queryset=queryset, alumno_uuid=alumno_uuid)
        except:
            pass

        return queryset

    def list(self, request, *args, **kwargs):
        param_format = request.GET.get('format', None)

        if param_format == 'datatables':
            # copio el codigo de super, para poder manipular su queryset filtrado
            queryset = self.filter_queryset(self.get_queryset())

            if queryset.exists():
                page = self.paginate_queryset(queryset)
                if page is not None:
                    serializer = self.get_serializer(page, many=True, context={"request": request})
                    return self.get_paginated_response(serializer.data)
            else:
                queryset = Horario.objects.none()

            serializer = self.get_serializer(queryset, many=True, context={"request": request})
            return Response(serializer.data)

        # obtenemos el queryset que en este punto ya debe estar filtrado
        qs = self.get_queryset()

        if not qs.exists():
            qs = Horario.objects.none()

        serializer = self.get_serializer(qs, many=True, context={"request": request})
        return Response(serializer.data)

    def get_permissions(self):
        """
        Permisos del view segun la accion que se realiza.
        """
        permission_classes = [permissions.AllowAny]
        return [permission() for permission in permission_classes]


class AsistenciaViewSet(viewsets.ModelViewSet):
    errors = []
    serializer_class = serializers.AsistenciaSerializer
    """
    ViewSet para operaciones sobre el modelo Asistencia.
    """

    def get_queryset(self):
        alumno_uuid = self.request.GET.get('alumno_uuid', None)

        queryset = Asistencia.objects.all()
        try:
            if alumno_uuid:
                if not common_models.Alumno.objects.filter(uuid=alumno_uuid).exists():
                    queryset = Asistencia.objects.none()
                else:
                    queryset = queryset.filter(alumno__uuid=alumno_uuid)

                    # si el list se realiza de otra forma que no sea el datatables
                    if self.request.GET.get('format', None) is None:
                        offset = int(self.request.GET.get('offset', 0))

                        # Limitar la cantidad de resultados?
                        limite_servidor = 30
                        limite_cliente = self.request.GET.get('limite_cliente', str(limite_servidor))
                        limite = min(int(limite_cliente), limite_servidor)

                        # Ordenar por fecha de noticia?
                        orden = self.request.GET.get('orden', 'ASC')
                        # Traer solo noticias de una categoria?
                        busqueda = self.request.GET.get('busqueda', None)

                        if busqueda:
                            pass
                        
                        if orden:
                            if orden == 'ASC':
                                queryset = queryset.order_by('created_at')

                            elif orden == 'DESC':
                                queryset = queryset.order_by('-created_at')

                            else:
                                raise ValidationError("Parametro de consulta 'orden' debe ser 'ASC' o 'DESC'.")

                        try:
                            queryset = queryset[offset:int(offset + limite)]
                        except ValueError:
                            raise ValidationError("Parametro de consulta 'limite' debe ser un entero.")

        except:
            pass

        return queryset

    def list(self, request, *args, **kwargs):
        param_format = request.GET.get('format', None)

        if param_format == 'datatables':
            # copio el codigo de super, para poder manipular su queryset filtrado
            queryset = self.filter_queryset(self.get_queryset())

            if queryset.exists():
                page = self.paginate_queryset(queryset)
                if page is not None:
                    serializer = self.get_serializer(page, many=True, context={"request": request})
                    return self.get_paginated_response(serializer.data)
            else:
                queryset = Horario.objects.none()

            serializer = self.get_serializer(queryset, many=True, context={"request": request})
            return Response(serializer.data)

        # obtenemos el queryset que en este punto ya debe estar filtrado
        qs = self.get_queryset()

        if not qs.exists():
            qs = Asistencia.objects.none()

        serializer = self.get_serializer(qs, many=True, context={"request": request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['post'], detail=False)
    def registrar(self, request, pk=None):
        # Validar el payload recibido con el serializer
        serializer = serializers.RegistroAsistenciaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Crear un usuario con los datos recibidos
        asistencia = common_models.Asistencia()
        asistencia.alumno = common_models.Alumno.objects.get(uuid=serializer.validated_data['uuid_alumno'])
        asistencia.horario = common_models.Horario.objects.get(uuid=serializer.validated_data['uuid_horario'])
        asistencia.save()

        # Responder con el usuario recientemente creado
        serializer = serializers.AsistenciaSerializer(asistencia, context={"request": request})

        return Response(serializer.data)

    def get_permissions(self):
        """
        Permisos del view segun la accion que se realiza.
        """
        permission_classes = [permissions.AllowAny]
        return [permission() for permission in permission_classes]


class NotificacionViewSet(viewsets.ModelViewSet):
    errors = []
    serializer_class = serializers.NotificacionSerializer
    """
    ViewSet para operaciones sobre el modelo Notificacion.
    """

    def get_queryset(self):
        alumno_uuid = self.request.GET.get('alumno_uuid', None)
        todos = self.request.GET.get('todos', 'False')
        fecha_ultimo_request = self.request.GET.get('fecha_ultimo_request', None)

        if fecha_ultimo_request is not None:
            try:
                fecha_ultimo_request = datetime.strptime(fecha_ultimo_request, '%Y-%m-%d %H:%M:%S')
            except Exception as e:
                print(e)
                raise ValidationError("Formato incorrecto de fecha. Se espera [%Y-%m-%d %H:%M:%S]")

        return manager.get_notificaciones_alumno(queryset=None, alumno_uuid=alumno_uuid, todos=todos, fecha_ultimo_request=fecha_ultimo_request)


    def list(self, request, *args, **kwargs):
        param_format = request.GET.get('format', None)

        # obtenemos el queryset que en este punto ya debe estar filtrado
        qs = self.get_queryset()

        if not qs.exists():
            qs = Notificacion.objects.none()

        serializer = self.get_serializer(qs, many=True, context={"request": request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_permissions(self):
        """
        Permisos del view segun la accion que se realiza.
        """
        permission_classes = [permissions.AllowAny]
        return [permission() for permission in permission_classes]
