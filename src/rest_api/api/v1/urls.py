from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include

from rest_api.api.v1 import views

app_name = 'rest_api'

urlpatterns = [
    #url(r'^password-reset/', include('django_rest_passwordreset.urls', namespace='password-reset')),
    #url(r'^reset-password/$', views.reset_password, name='reset-password'),
    url(r'^login/$', views.CustomAuthToken.as_view()),
    #url(r'^post_cambiar_password/$', views.PostCambiarPassword.as_view(), name='post_cambiar_password'),
]


router = DefaultRouter()

router.register(r'horario', views.HorarioViewSet, base_name='horario')
router.register(r'asistencia', views.AsistenciaViewSet, base_name='asistencia')
router.register(r'notificacion', views.NotificacionViewSet, base_name='notificacion')
urlpatterns += router.urls
