import math
import datetime
import dateutil.parser
from django.db import IntegrityError, models as dbmodels
from django.db.models.expressions import Value
from django.db.models import F, Q
from rest_framework import serializers
from rest_framework.compat import authenticate
from common import models as common_models
from common import manager
from django.shortcuts import get_object_or_404


class CustomAuthTokenSerializer(serializers.Serializer):
    username = serializers.CharField(label="Username")
    password = serializers.CharField(
        label="Password",
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(request=self.context.get('request'),
                                username=username, password=password)

            # The authenticate call simply returns None for is_active=False
            # users. (Assuming the default ModelBackend authentication
            # backend.)
            if not user:
                msg = 'Usuario o contraseña incorrectos'
                raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user

        return attrs


class PersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = common_models.Persona
        fields = [
            'uuid',
            'nombre',
            'apellido',
            'sexo',
            'ci',
            'fecha_nacimiento',
            'direccion',
        ]


class ProfesorSerializer(serializers.ModelSerializer):
    persona = PersonaSerializer(many=False, read_only=True)
    class Meta:
        model = common_models.Profesor
        fields = [
            'uuid',
            'persona',
        ]
        extra_kwargs = {
            'persona': {'read_only': True},
            'uuid': {'read_only': True},
        }

class AlumnoSerializer(serializers.ModelSerializer):
    persona = PersonaSerializer(many=False, read_only=True)
    class Meta:
        model = common_models.Profesor
        fields = [
            'uuid',
            'persona',
        ]
        extra_kwargs = {
            'persona': {'read_only': True},
            'uuid': {'read_only': True},
        }


class MateriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = common_models.Materia
        fields = [
            'uuid',
            'nombre',
        ]
        extra_kwargs = {
            'nombre': {'read_only': True},
            'uuid': {'read_only': True},
        }


class ClaseSerializer(serializers.ModelSerializer):
    materia = MateriaSerializer(many=False, read_only=True)
    profesor = ProfesorSerializer(many=False, read_only=True)

    class Meta:
        model = common_models.Clase
        fields = [
            'uuid',
            'seccion',
            'materia',
            'profesor'
        ]
        extra_kwargs = {
            'seccion': {'read_only': True},
            'materia': {'read_only': True},
            'profesor': {'read_only': True},
            'uuid': {'read_only': True},
        }


class BeaconSerializer(serializers.ModelSerializer):
    class Meta:
        model = common_models.Beacon
        fields = [
            'uuid',
            'beacon_id',
            'mac',
        ]
        extra_kwargs = {
            'uuid': {'read_only': True},
            'beacon_id': {'read_only': True},
            'mac': {'read_only': True},
        }


class AulaSerializer(serializers.ModelSerializer):
    beacons = BeaconSerializer(many=True)

    class Meta:
        model = common_models.Aula
        fields = [
            'uuid',
            'nombre',
            'beacons',
        ]
        extra_kwargs = {
            'uuid': {'read_only': True},
            'nombre': {'read_only': True},
            'beacons': {'read_only': True},
        }


class HorarioSerializer(serializers.ModelSerializer):
    clase = ClaseSerializer(many=False, read_only=True)
    aula = AulaSerializer(many=False, read_only=True)
    minimo_escaneo = serializers.SerializerMethodField()

    class Meta:
        model = common_models.Horario
        fields = [
            'uuid',
            'dia',
            'hora_inicio',
            'hora_fin',
            'clase',
            'aula',
            'minimo_escaneo',
        ]
        extra_kwargs = {
            'uuid': {'read_only': True},
            'dia': {'read_only': True},
            'hora_inicio': {'read_only': True},
            'hora_fin': {'read_only': True},
            'clase': {'read_only': True},
            'aula': {'read_only': True},
        }

    def get_minimo_escaneo(self, obj):
        duracion_minutos = obj.duracion
        print(str(duracion_minutos))
        intervalo_request_minutos = 0.5
        porcentaje = obj.porcentaje/100
        print(str(porcentaje))
        cantidad_max_requests = duracion_minutos/intervalo_request_minutos
        print(str(cantidad_max_requests))
        minimo_escaneo = math.floor(porcentaje*cantidad_max_requests)
        print(str(minimo_escaneo))
        return minimo_escaneo


class RegistroAsistenciaSerializer(serializers.Serializer):
    uuid_horario = serializers.CharField(required=True)
    uuid_alumno = serializers.CharField(required=True)
    mac_beacon = serializers.CharField(required=True)

    def validate(self, data):
        horario = None
        alumno = None
        beacon = None

        try:
            horario = common_models.Horario.objects.get(uuid=data.get('uuid_horario'))
            alumno = common_models.Alumno.objects.get(uuid=data.get('uuid_alumno'))
            beacon = common_models.Beacon.objects.get(mac=data.get('mac_beacon'))
        except:
            pass

        if not horario:
            raise serializers.ValidationError("Horario no existe.")
        if not beacon:
            raise serializers.ValidationError("Beacon no existe.")
        if not alumno:
            raise serializers.ValidationError("Alumno no existe.")

        if not beacon.aula.id == horario.aula.id:
            raise serializers.ValidationError("Beacon no corresponde al horario.")


        horarios = manager.get_horarios_alumno(queryset=None, alumno_uuid=alumno.uuid)
        for h in horarios:
            if h.uuid == horario.uuid:
                today = datetime.datetime.now().date()
                tomorrow = today + datetime.timedelta(1)
                today_start = datetime.datetime.combine(today, datetime.time())
                today_end = datetime.datetime.combine(tomorrow, datetime.time())

                asistencia_existente = \
                    common_models.Asistencia\
                        .objects\
                        .filter(horario__uuid=h.uuid, alumno__uuid=alumno.uuid, created_at__lte=today_end, created_at__gte=today_start)\
                        .exists()

                if asistencia_existente:
                    raise serializers.ValidationError("La asistencia ya ha sido registrada previamente.")

                return data

        raise serializers.ValidationError("Alumno no inscripto en la materia.")


class AsistenciaSerializer(serializers.ModelSerializer):
    alumno = AlumnoSerializer(many=False, read_only=True)
    horario = HorarioSerializer(many=False, read_only=True)

    class Meta:
        model = common_models.Asistencia
        fields = [
            'uuid',
            'alumno',
            'horario',
            'created_at'
        ]
        extra_kwargs = {
            'alumno': {'read_only': True},
            'horario': {'read_only': True},
            'uuid': {'read_only': True},
            'created_at': {'read_only': True},
        }


class NotificacionSerializer(serializers.ModelSerializer):
    beacon = BeaconSerializer()

    class Meta:
        model = common_models.Notificacion
        fields = [
            'uuid',
            'beacon',
            'titulo',
            'mensaje',
            'fecha_inicio',
            'fecha_fin'
        ]
        extra_kwargs = {
            'beacon': {'read_only': True},
            'titulo': {'read_only': True},
            'mensaje': {'read_only': True},
            'fecha_inicio': {'read_only': True},
            'fecha_fin': {'read_only': True},
            'uuid': {'read_only': True},
        }