#!/usr/bin/env bash

# Si existen los archivos generados por celery, eliminarlos
# ya que (aparentemente) deberian removerse al finalizar los procesos
if [ -f ./celerybeat.pid ]; then
	rm celerybeat.pid
	echo 'Removing celerybeat.pid'
fi

if [ -f ./celerybeat-schedule ]; then
        rm celerybeat-schedule
        echo 'Removing celerybeat-schedule'
fi

# Iniciar los workers de celery, que son los procesos de celery que esperan
# recibir tasks para ejecutarlos
# Se puede llamar a un task:
#   1) Explicitamente: haciendo <task-name>.delay(arguments)
#   2) Periodicamente: con el decorator @periodic_task sobre el task
celery worker --app=common.tasks &

# Iniciar el celery beat, que es un proceso que se va a encargar de mandarle
# los workers los periodic tasks en el momento que corresponda.
celery -A markapp beat -l info

