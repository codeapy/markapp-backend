#!/bin/bash

# Start Gunicorn processes
echo Starting Gunicorn.
cd /src
exec gunicorn markapp.wsgi:application \
    --bind 0.0.0.0:8000 \
    --workers 4 \
    --timeout 360\
    --reload